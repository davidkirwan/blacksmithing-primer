=== Smelting Temperatures of Metals
The following table contains information about the smelting temperatures for some of the common metals.

[cols="1,1"]
|===
| Metal | Temperature C

| Aluminimum | 660.3
| Brass | 905 - 1025
| Bronze | 913
| Cobalt | 1495
| Copper | 1085
| Gold | 1063
| Iron | 1127 - 1593
| Lead | 327.5
| Magnesium | 650
| Nickel | 1453
| Palladium | 1555
| Platinum | 1770
| Silver | 961
| Steel | 1510
| Stainless Steel | 1540
| Tin | 231.9
| Titanium | 1668
| Tungsten | 3422
| Zinc | 420
|===

