=== Forging Techniques
General notes about the general Forging techniques. Eg: bending, punching, drawing, upsetting, shrinking <<arzt22>>.

==== Forge Configuration
- some value

==== Prevent deforming when drifting
When drifting holes with larger drifts, especially larger than the pritchel hole, you can use the technique of drifting at the corners of the hardy hole, each blow, move to the next corner.

==== Safety
Safety notes specifically related to forging.

