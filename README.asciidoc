== Blacksmithing Primer
A primer on Blacksmithing and Smelting.

Build the book using Asciidoctor:

----
bundle install
bundle exec asciidoctor-pdf blacksmithing_primer.asciidoc
----
