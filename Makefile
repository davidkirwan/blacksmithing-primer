####################################################################################################
# @author David Kirwan https://gitlab.com/davidkirwan/blacksmithing-primer
# @description Makefile for building the pdf for the Blacksmithing Primer
#
# @usage Run `make` to get full usage instructions
#
# @date 2022-02-12
####################################################################################################
.DEFAULT_GOAL:=help
PROJECT=blacksmithing-primer
AUTHOR=David-Kirwan
##############################

.PHONY: help
help: ## Show this help screen
	@echo 'Usage: make <OPTIONS> ... <TARGETS>'
	@echo ''
	@echo 'Available targets are:'
	@echo ''
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

##############################
##@ All

.PHONY: all
all: dependencies build ## Pull dependencies, builds artifacts


##############################
##@ Install dependencies for asciidoctor-pdf


.PHONY: dependencies
dependencies: ## Install the dependencies for asciidoctor-pdf
	bundle install

##############################
##@ Build PDF

.PHONY: build
build: ## Compile and build the pdf
	bundle exec asciidoctor-pdf blacksmithing_primer.asciidoc

